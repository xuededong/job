package com.skz.job;

import com.skz.job.service.JobService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JobApplicationTests {
    @Autowired
    private JobService jobService;

    @Test
    public void contextLoads() {
        jobService.save();
    }
}
