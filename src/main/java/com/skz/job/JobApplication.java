package com.skz.job;

import com.skz.job.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 */
@SpringBootApplication
@EnableScheduling
public class JobApplication {
//    @Autowired
//    private JobService jobService;
//
//    /**
//     * 定时抓取
//     */
//    @Scheduled(cron = "0 1 0 * * *")
//    public void spiderBoss() {
//        jobService.save();
//    }

    public static void main(String[] args) {
        SpringApplication.run(JobApplication.class, args);
    }
}
