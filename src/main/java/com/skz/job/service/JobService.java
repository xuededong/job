package com.skz.job.service;

import com.skz.job.domain.Job;
import com.skz.job.repository.JobRepository;
import com.skz.job.spider.BossZhiPinSpider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Spider;

import java.util.Date;
import java.util.List;

/**
 * @author: 宋开宗
 * @create: 2019-02-23 13:02
 **/
@Service
public class JobService {

    @Autowired
    private JobRepository jobRepository;
    private String listUrl = "https://www.zhipin.com/c101120100-p100101/?period=5&page=";
    private int startPage = 1;

    public void save() {
        Spider.create(new BossZhiPinSpider(listUrl, startPage)).addUrl(listUrl + startPage).addPipeline((resultItems, task) -> {
            Object job = resultItems.get("job");
            if (job != null) {
                Job bossJob = (Job) job;
                jobRepository.save(bossJob);
                System.out.println("save:" + bossJob.getCompanyName());
            }
        }).thread(10).run();
    }

    public List<Job> findTodayJobs() {
        return jobRepository.findByCreateDate(new Date());
    }

    public void del(Job job) {
        jobRepository.delete(job);
    }
}
