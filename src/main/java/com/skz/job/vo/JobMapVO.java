package com.skz.job.vo;

import lombok.Data;

/**
 * @author: 宋开宗
 * @create: 2019-02-19 14:19
 **/
@Data
public class JobMapVO {
    private String company;
    private String longitude;
    private String url;

    public JobMapVO(String company, String longitude, String url) {
        this.company = company;
        this.longitude = longitude;
        this.url = url;
    }
}
