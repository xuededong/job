package com.skz.job.domain;

import lombok.Data;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: 宋开宗
 * @create: 2019-02-19 11:55
 **/
@Entity
@Table(name = "t_boss_job")
@Data
public class Job extends AbstractPersistable<Integer> {
    private String companyName;
    /**
     * 行业
     */
    private String trade;
    @Column(length = 2000)
    private String description;
    /**
     * 经纬度
     */
    private String longitude;
    /**
     * 区
     */
    private String district;
    /**
     * 通过经纬度逆解析的地址，获取其街道或所在的写字楼而忽略具体单元号，门牌号
     */
    private String simpleAddress;
    /**
     * 详细地址
     */
    private String address;
    private String url;
    /**
     * 来源，boss直聘，智联，拉钩
     */
    private String source;
    /**
     * 职位，1.产品，2.产品助理，3.java
     */
    private int type;
    @Temporal(TemporalType.DATE)
    private Date createDate = new Date();

    public Job(String companyName, String trade, String description, String longitude, String district,
               String simpleAddress, String address, String url, String source, int type) {
        this.companyName = companyName;
        this.trade = trade;
        this.description = description;
        this.longitude = longitude;
        this.district = district;
        this.simpleAddress = simpleAddress;
        this.address = address;
        this.url = url;
        this.source = source;
        this.type = type;
    }

    public Job() {
    }
}
