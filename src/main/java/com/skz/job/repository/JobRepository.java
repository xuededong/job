package com.skz.job.repository;

import com.skz.job.domain.Job;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * @author: 宋开宗
 * @create: 2019-02-19 12:57
 **/
public interface JobRepository extends JpaRepository<Job, Integer> {
    List<Job> findByCreateDate(Date date);
}
